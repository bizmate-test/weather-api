# weather-api



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bizmate-test/weather-api.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/bizmate-test/weather-api/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Welcome to Bizmate Weather App

## Repository Name
Weather-api

## Description
This repository serves as the API (back end) of the Bizmate Weather App.

## Why this code implementation is the best?
The back end code implementation of this project is the best because of the following reasons
1. It uses the latest version of laravel (version 10) and it uses passport for the token base authentication.

2. Framework Consistency: Laravel provides a consistent and well-structured framework for building RESTful APIs. It follows the MVC (Model-View-Controller) pattern, making it easier to organize your code and maintain it over time.

3. Eloquent ORM: Laravel comes with an elegant and expressive ORM (Object-Relational Mapping) called Eloquent. This simplifies database operations by allowing you to interact with your database using PHP syntax, rather than writing complex SQL queries.

4. Middleware: Middleware in Laravel allows you to filter and manipulate HTTP requests entering your application. You can use middleware to perform actions like logging, authentication, and request validation before they reach your API routes.

5. Validation: Laravel provides a robust validation system, allowing you to validate incoming data easily. This is crucial for ensuring that your API receives valid and well-structured requests.

6. Routing: Laravel's routing system is flexible and allows you to define API routes with clear and expressive syntax. You can easily define routes for different HTTP methods (GET, POST, PUT, DELETE) and URL patterns.

7. Passport. As mentioned earler this system uses Laravel passport. Laravel Passport is a package for the Laravel PHP framework that provides a full OAuth2 server implementation for authentication in web applications and the creation of secure API endpoints. It simplifies the process of implementing authentication mechanisms, particularly when you need to handle user authentication, token generation, and API security. This project utilizes the use of passport's personal access token.

8. Highly Scalable. This API can be hosted in a separate server for scalability and can also be deployed with a load balancer configuration in placed. Since it uses Laravel passport for its authentication and Laravel passport is by design uses database to store all its access tokens then it enables this API to be hosted in multiple servers with a centralize token base authentication mechanism.

