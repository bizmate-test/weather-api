<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

final class HttpStatusCode extends Enum
{
    //Success Statuses
    const SuccessOK = 200;
    const SuccessCreated = 201;
    const SuccessAccepted = 202;

    //Clients Errors Statuses
    const ClientErrorBadRequest=400;
    const ClientErrorUnauthorized=401;
    const ClientErrorForbidden=403;
    const ClientErrorNotFound=404;
    const ClientErrorMethodNotAllowed=405;
    const ClientErrorRequestTimeout=408;
    const ClientErrorConflict=409;
    const ClientErrorGone=410;
    const ClientErrorPayloadTooLarge=413;
    const ClientErrorTooManyRequests=429;

    //Server Errors Statuses
    const ServerErrorInternalServerError=500;
    const ServerErrorBadGateway=502;

}
