<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SystemSetting extends Enum
{
    const AllowedPageMinValue = 1;
    const AllowedPageMaxValue = 50000;
}
