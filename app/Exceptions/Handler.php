<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    //set the error logging mechanism to daily and log it using daily log file
    public function report(Throwable $exception)
    {
        // Log the exception using the 'daily_error_log' channel
        Log::channel('daily')->error($exception->getMessage(), ['exception' => $exception]);

        parent::report($exception);
    }
}
