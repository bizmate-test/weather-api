<?php

use App\Models\Category;
use App\Models\Country;
use App\Models\Manufacturer;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

if (!function_exists('burtTest')) {
    function burtTest(){
        echo "Hello this is just a helper test created by burt";
    }

}


if (!function_exists('getCountry')) {
    function getCountry($id){
        if($id===null)
        {
            return null;
        }else{
            return Country::find($id);
        }
    }

}

if (!function_exists('getRole')) {
    function getRole($id){
        return Role::find($id);
    }

}

if (!function_exists('getManufacturer')) {
    function getManufacturer($id){
        return Manufacturer::find($id);
    }

}

if (!function_exists('getCBSS')) {
    function getCBSS($catID,$brandID,$supID,$storeID){
        $query = DB::query();
        if(!empty($catID)){
            $query->selectraw('(select name from categories where id = ? and deleted_at IS NULL) as category',[$catID]);
       }
        if(!empty($brandID)){
             $query->selectraw('(select name from brands where id = ? and deleted_at IS NULL) as brand',[$brandID]);
        }
         if(!empty($supID)){
             $query->selectraw('(select name from suppliers where id = ? and deleted_at IS NULL) as supplier',[$supID]);
         }
         if(!empty($storeID)){
             $query->selectraw('(select name from stores where id = ? and deleted_at IS NULL) as store',[$storeID]);
         }

         $data=$query->first();
         //dd($data);
         return $data;
    }

}

if (!function_exists('GetSqlWithBindings')) {

    /**
     * description
     *
     * @param
     * @return
     */
 function GetSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}
?>
