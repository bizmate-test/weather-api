<?php

namespace App\Http\Controllers;

use App\Enums\AccountType;
use App\Models\Account;
use App\Http\Requests\StoreAccountRequest;
use App\Http\Requests\RegisterAccountRequest;
use App\Http\Requests\UpdateAccountRequest;

use App\Exports\AccountExport;
use App\Http\Resources\AccountResource;
use App\Http\Requests\ViewAccountRequest;
use Illuminate\Database\QueryException;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;
use App\Enums\FileName;
use App\Enums\SystemData;
use App\Http\Requests\ActivateAccountRequest;
use App\Mail\ActivateAccountMail;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AccountController extends Controller
{

    public function index(ViewAccountRequest $request)
    {

        //Validation is automatically done by
        //the ViewRequest class

        //extract page size
        $per_page = $request->input('per_page');

        $wildCard = ($request->filter_activatewildcard == 1) ? '%' : '';

        //Let's build our queries
        if($request->count_only){
            $dataQuery = Account::CustomCount();
        }else{
            $dataQuery = Account::CustomSearch();
        }

        //Check for Filters Begin -------------------------------------------------------//

        //Filter All COlumn
        if ($request->filter_allcolumn != '') {
            $dataQuery->whereraw('concat(first_name,middle_name,last_name,email,website,phone1,phone2,phone3,fax,address1,address2,zip_postal_code,city,subnational_entity,country_id,note,status,tag,created_at) LIKE ?', '%' . $request->filter_allcolumn . '%');
        }

        //Filter ID
        if ($request->filter_id != '') {
            $dataQuery->where('accounts.id', 'LIKE', $wildCard . $request->filter_id . '%');
        }

        //Filter first_name
        if ($request->filter_first_name != '') {
            $dataQuery->where('first_name', 'LIKE', $wildCard . $request->filter_first_name . '%');
        }

        //Filter middle_name
        if ($request->filter_middle_name != '') {
            $dataQuery->where('middle_name', 'LIKE', $wildCard . $request->filter_middle_name . '%');
        }

        //Filter last_name
        if ($request->filter_last_name != '') {
            $dataQuery->where('last_name', 'LIKE', $wildCard . $request->filter_last_name . '%');
        }

        //Filter Email
        if ($request->filter_email != '') {
            $dataQuery->where('email', 'LIKE', $wildCard . $request->filter_email . '%');
        }

        //Filter Website
        if ($request->filter_website != '') {
            $dataQuery->where('website', 'LIKE', $wildCard . $request->filter_website . '%');
        }

        //Filter Phone1
        if ($request->filter_phone1 != '') {
            $dataQuery->where('phone1', 'LIKE', $wildCard . $request->filter_phone1 . '%');
        }

        //Filter Phone2
        if ($request->filter_phone2 != '') {
            $dataQuery->where('phone2', 'LIKE', $wildCard . $request->filter_phone2 . '%');
        }

        //Filter Phone3
        if ($request->filter_phone3 != '') {
            $dataQuery->where('phone3', 'LIKE', $wildCard . $request->filter_phone3 . '%');
        }

        //Filter fax
        if ($request->filter_fax != '') {
            $dataQuery->where('fax', 'LIKE', $wildCard . $request->filter_fax . '%');
        }

        //Filter address1
        if ($request->filter_address1 != '') {
            $dataQuery->where('address1', 'LIKE', $wildCard . $request->filter_address1 . '%');
        }

        //Filter address2
        if ($request->filter_address2 != '') {
            $dataQuery->where('address2', 'LIKE', $wildCard . $request->filter_address2 . '%');
        }

        //Filter zip_postal_code
        if ($request->filter_zip_postal_code != '') {
            $dataQuery->where('zip_postal_code', 'LIKE', $wildCard . $request->filter_zip_postal_code . '%');
        }

        //Filter city
        if ($request->filter_city != '') {
            $dataQuery->where('city', 'LIKE', $wildCard . $request->filter_city . '%');
        }

        //Filter subnational_entity
        if ($request->filter_subnational_entity != '') {
            $dataQuery->where('subnational_entity', 'LIKE', $wildCard . $request->filter_subnational_entity . '%');
        }

        //Filter country
        if ($request->filter_country_id != '') {
            $dataQuery->where('country', 'LIKE', $wildCard . $request->filter_country . '%');
        }

        //Filter note
        if ($request->filter_note != '') {
            $dataQuery->where('note', 'LIKE', $wildCard . $request->filter_note . '%');
        }

        //Filter Status
        if ($request->filter_status != '') {
            $dataQuery->where('status', 'LIKE', $wildCard . $request->filter_status . '%');
        }

        //Filter tag
        if ($request->filter_tag != '') {
            $dataQuery->where('tag', 'LIKE', $wildCard . $request->filter_tag . '%');
        }

        //Filter Created
        if ($request->filter_created_at != '') {
            $dataQuery->where('accounts.created_at', 'LIKE', $wildCard . $request->filter_created_at . '%');
        }

        //Check for Filters End -------------------------------------------------------//

        //Apply Sorting Mechanism Begin ----------------------------------------------//

        if(!$request->count_only){

            //Sort by ID
            if ($request->sort_id) {
                $dataQuery->orderBy('accounts.id', ($request->sort_id == 1) ? "asc" : "desc");
            }

            //Sort by first_name
            if ($request->sort_first_name) {
                $dataQuery->orderBy('first_name', ($request->sort_first_name == 1) ? "asc" : "desc");
            }

            //Sort by middle_name
            if ($request->sort_middle_name) {
                $dataQuery->orderBy('middle_name', ($request->sort_middle_name == 1) ? "asc" : "desc");
            }

            //Sort by last_name
            if ($request->sort_name) {
                $dataQuery->orderBy('name', ($request->sort_name == 1) ? "asc" : "desc");
            }


            //Sort by email
            if ($request->sort_email) {
                $dataQuery->orderBy('email', ($request->sort_email == 1) ? "asc" : "desc");
            }

            //Sort by website
            if ($request->sort_website) {
                $dataQuery->orderBy('website', ($request->sort_website == 1) ? "asc" : "desc");
            }

            //Sort by phone1
            if ($request->sort_phone1) {
                $dataQuery->orderBy('phone1', ($request->sort_phone1 == 1) ? "asc" : "desc");
            }

            //Sort by phone2
            if ($request->sort_phone2) {
                $dataQuery->orderBy('phone2', ($request->sort_phone2 == 1) ? "asc" : "desc");
            }

            //Sort by phone3
            if ($request->sort_phone3) {
                $dataQuery->orderBy('phone3', ($request->sort_phone3 == 1) ? "asc" : "desc");
            }

            //Sort by fax
            if ($request->sort_fax) {
                $dataQuery->orderBy('fax', ($request->sort_fax == 1) ? "asc" : "desc");
            }

            //Sort by address1
            if ($request->sort_address1) {
                $dataQuery->orderBy('address1', ($request->sort_address1 == 1) ? "asc" : "desc");
            }

            //Sort by address2
            if ($request->sort_address2) {
                $dataQuery->orderBy('address2', ($request->sort_address2 == 1) ? "asc" : "desc");
            }

            //Sort by zip_postal_code
            if ($request->sort_zip_postal_code) {
                $dataQuery->orderBy('zip_postal_code', ($request->sort_zip_postal_code == 1) ? "asc" : "desc");
            }

            //Sort by city
            if ($request->sort_city) {
                $dataQuery->orderBy('city', ($request->sort_city == 1) ? "asc" : "desc");
            }

            //Sort by subnational_entity
            if ($request->sort_subnational_entity) {
                $dataQuery->orderBy('sort_subnational_entity', ($request->sort_subnational_entity == 1) ? "asc" : "desc");
            }

            //Sort by country_id
            if ($request->sort_country_id) {
                $dataQuery->orderBy('country_id', ($request->sort_country_id == 1) ? "asc" : "desc");
            }


            //Sort by note
            if ($request->sort_note) {
                $dataQuery->orderBy('note', ($request->sort_note == 1) ? "asc" : "desc");
            }

            //Sort by status
            if ($request->sort_status) {
                $dataQuery->orderBy('status', ($request->sort_status == 1) ? "asc" : "desc");
            }

            //Sort by tag
            if ($request->sort_tag) {
                $dataQuery->orderBy('tag', ($request->sort_tag == 1) ? "asc" : "desc");
            }

            //Sort by created_at
            if ($request->sort_created_at) {
                $dataQuery->orderBy('accounts.created_at', ($request->sort_created_at == 1) ? "asc" : "desc");
            }

        }
        //Apply Sorting Mechanism End -----------------------------------------==-----//


        try {

            //Lastly Execute Query and Paginate the result
            //paginate also perform counting of records
            //if you have 1 million record it is still fast as long as
            //you make use of proper table joins
            //use leftjoin if some tables most of the time has less than 1000 records
            // data from the main table
            //$dataResult = $dataQuery->paginate($per_page);

            if($request->count_only){
              $dataResult = $dataQuery->first();
            }else{
              $dataResult = $dataQuery->Simplepaginate($per_page);
            }

        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }

        //if the request is count_only then compute the last page
        //then automatically return the count results.
        if($request->count_only){
            if (intval($dataResult->getAttribute('count')) >1 || intval($request->per_page) >1)
            {
                $lastPage =intval(ceil( intval($dataResult->getAttribute('count')) / intval($request->per_page) ));
            }else{
                $lastPage = 1;
            }

            return response()->json(
                [
                    SystemData::ResultCount => $dataResult->getAttribute('count'),
                    SystemData::LastPage => $lastPage,
                    SystemData::Success => false
                ]);
        }

        //if the request is NOT count only then continue to perform the code below
        //Check if dataResult has record, if not throw message
        if ($dataResult->count() <= 0) {
            return response()->json(
                [
                    SystemData::Message => SystemMessage::AccountNoRecordFound,
                    SystemData::Success => false
                ],
                HttpStatusCode::ClientErrorNotFound
            );
        }

        //Check if this request is for EXPORT.
        //If export_to is NOT '' means YES then export the data to target format
        if ($request->export_to != '') {
            switch (strtoupper($request->export_to)) {
                case FileName::EXCELFile:
                    return (new AccountExport($dataResult))->download(FileName::AccountExportFile . FileName::EXCELFileFormat, \Maatwebsite\Excel\Excel::XLSX, null);
                    break;
                case FileName::CSVFile:
                    return (new AccountExport($dataResult))->download(FileName::AccountExportFile . FileName::CSVFileFormat, \Maatwebsite\Excel\Excel::CSV, null);
                    break;

                default:
                    return response()->json(
                        [
                            SystemData::Message => SystemMessage::FileFormatNotSupported,
                            SystemData::Success => false
                        ],
                        HttpStatusCode::ClientErrorBadRequest
                    );
            }
        }

        return AccountResource::collection($dataResult)->additional([
            SystemData::Message => SystemMessage::AccountRecordRetrieved,
            SystemData::Success => true
        ]);
    }

    public function all()
    {
        //Show all records including those mark deleted
        $dataResult = Account::CustomSearch()->withTrashed()
                      ->simplepaginate(100);

        return AccountResource::collection($dataResult)->additional([
            SystemData::Message => SystemMessage::AccountRecordRetrieved,
            SystemData::Success => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store(StoreAccountRequest $request)
    {

        //Validation is automatically handled
        //by StoreUserRequest

        //Pick Up only all those data from the request
        //that are needed to save
        $data = [
            "first_name" => $request->first_name,
            "middle_name" => $request->middle_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "website" => $request->website,
            "phone1" => $request->phone1,
            "phone2" => $request->phone2,
            "phone3" => $request->phone3,
            "fax" => $request->fax,
            "address1" => $request->address1,
            "address2" => $request->address2,
            "zip_postal_code" => $request->zip_postal_code,
            "city" => $request->city,
            "subnational_entity" => $request->subnational_entity,
            "country_id" => $request->country_id,
            "note" => $request->note,
            "status" => 0,
            "tag" => $request->tag,
        ];

        try {
            //Check if email already exist in the account table
            $account = Account::CustomSearch()->where('email','=',$request->email)->first();

            if (!is_null($account)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountAlreadyExist,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorBadRequest
                );
            }

            //check if email also exist in user table
            $user = User::CustomSearch()->where('email','=',$request->email)->first();

            if (!is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountAlreadyExist,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorBadRequest
                );
            }

            //Execute and return the newly created record
            $account = Account::create($data);
            //Get the Country
            $account->country = getCountry($account->country_id)?->country;

            //return the resource with additional element
            return AccountResource::collection([$account])->additional([
                SystemData::Message => SystemMessage::AccountRecordCreated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    public function register(RegisterAccountRequest $request)
    {

        //Validation is automatically handled
        //by StoreUserRequest

        //Pick Up only all those data from the request
        //that are needed to save
        $accountData = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "address1" => $request->address1,
            "city" => $request->city,
            "zip_postal_code" => $request->zip_postal_code,
            "subnational_entity" => $request->subnational_entity,
            "country_id" => $request->country_id,
            "status" => 0,
        ];

        $userData = [
            "type" => AccountType::Subscriber,
            "account_id" =>0,
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "name" => $request->first_name." ".$request->last_name,
            "email" => $request->email,
            "address1" => $request->address1,
            "city" => $request->city,
            "zip_postal_code" => $request->zip_postal_code,
            "subnational_entity" => $request->subnational_entity,
            "country_id" => $request->country_id,
            "password" => Hash::make($request->password),
            "status" => 0,
        ];

        try {
            //Check if email already exist in the account table
            $account = Account::CustomSearch()->where('email','=',$request->email)->first();

            if (!is_null($account)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountAlreadyExist,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorBadRequest
                );
            }

            //check if email also exist in user table
            $user = User::CustomSearch()->where('email','=',$request->email)->first();

            if (!is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountAlreadyExist,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorBadRequest
                );
            }

            //Create the account and return the newly created record
            $account = Account::create($accountData);
            //Get the Country
            $account->country = getCountry($account->country_id)?->country;

            //Create a user record for the newly created account
            $userData['account_id']=$account->id;
            $user = User::create($userData);

            //add these data before sending the email
            $accountData['activation_url'] = env('FRONT_END_APP_URL');
            $accountData['activation_code'] =  fake()->numberBetween($min = 1111111, $max = 99999999);

            //send email for activation
            Mail::to($accountData['email'])->send(new ActivateAccountMail($accountData));

            //create activation code
            User::where('email', $accountData['email'])->limit(1)->update(['activation_code' =>  $accountData['activation_code']]);

            //return the resource with additional element
            return AccountResource::collection([$account])->additional([
                SystemData::Message => SystemMessage::AccountRecordCreated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    public function activate(ActivateAccountRequest $request)
    {
        //Validation is automatically handled
        //by StoreUserRequest

        //Pick Up only all those data from the request
        //that are needed to save
        // $userData = [
        //     "email" => $request->email,
        //     "activation_code" => $request->activation_code,
        // ];

        try {
            //Check if email and activation code exist in user table
            $user = User::where('email','=',$request->email)
                            ->where('activation_code','=',$request->activation_code)
                            ->first();

            if (is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountActivationFailed,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorBadRequest
                );
            }

            //activate the user
            $user->update(['account_status'=>1,'status'=>1,'activation_code' => null,]);

            //activate the account
            Account::where('email','=',$request->email)
            ->where('id','=',$user->account_id)
            ->update(['status'=>1]);


            //return a message that account has been activated
            return response()->json(
                [
                    SystemData::Message => SystemMessage::AccountHasBeenActivated,
                    SystemData::Success => true
                ]
            );

        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Display the specified resource.
     */

    public function show($id)
    {
        try {
           $account = Account::CustomSearch()->find($id);

            if (is_null($account)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //show found record as object
            return response()->json([
                SystemData::Data => AccountResource::make($account),
                SystemData::Message => SystemMessage::AccountRecordFound,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Update the specified resource in storage.
     */

    public function update(UpdateAccountRequest $request, $id)
    {
        //Validation is automatically handled
        //by the UpdateUserRequest

        //Pick Up only all those data from the request
        //that are needed to update
        $data = [
            "first_name" => $request->first_name,
            "middle_name" => $request->middle_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "website" => $request->website,
            "phone1" => $request->phone1,
            "phone2" => $request->phone2,
            "phone3" => $request->phone3,
            "fax" => $request->fax,
            "address1" => $request->address1,
            "address2" => $request->address2,
            "zip_postal_code" => $request->zip_postal_code,
            "city" => $request->city,
            "subnational_entity" => $request->subnational_entity,
            "country_id" => $request->country_id,
            "note" => $request->note,
            "status" => $request->status,
            "tag" => $request->tag,
        ];

        //Find and return the found record
        try {
            $account = Account::CustomSearch()->find($id);

            if (is_null($account)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute update to update the record
            $account->update($data);
            $account = Account::CustomSearch()->find($id);

            //show found record as object
            return response()->json([
                SystemData::Data => AccountResource::make($account),
                SystemData::Message => SystemMessage::AccountRecordUpdated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Remove the specified resource from storage.
     */

    public function destroy($id)
    {

        try {
            //Before delete make sure to check first if this client ID
            //is not yet been used as referrence by accounts record

            if (!is_null(null)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountCanNotDelete . $id . SystemMessage::AlreadyBeenUsed,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

           $account = Account::CustomSearch()->find($id);

            if (is_null($account)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute Delete and return the deleted record
            $account->delete();


            //show deleted record as object
            return response()->json([
                SystemData::Data => AccountResource::make($account),
                SystemData::Message => SystemMessage::AccountRecordDeleted,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }


    public function undestroy($id)
    {
        try {

            $account = Account::CustomSearch()->onlyTrashed()->find($id);

            if (is_null($account)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::AccountID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            $account->restore();

            //show undeleted record as object
            return response()->json([
                SystemData::Data => AccountResource::make($account),
                SystemData::Message => SystemMessage::AccountRecordRestored,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }


    public function stats()
    {
        try {

            $activeRecords = Account::count();
            $softDeletedRecords = Account::onlyTrashed()->count();
            $totalRecords = $activeRecords + $softDeletedRecords;

            //return the Resource with additional element
            return response()->json(
                [
                    SystemData::Data => [
                        SystemData::ActiveRecords => $activeRecords,
                        SystemData::SoftDeletedRecords => $softDeletedRecords,
                        SystemData::TotalRecords => $totalRecords
                    ],
                    SystemData::Message => SystemMessage::StatsRetrieved,
                    SystemData::Success => true
                ],
                HttpStatusCode::SuccessOK
            );
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }
}
