<?php

namespace App\Http\Controllers;
use App\Http\Requests\LoginUserRequest;
use Illuminate\Support\Facades\Auth;
use App\Enums\SystemMessage;
use App\Enums\SystemData;
use App\Enums\HttpStatusCode;
use App\Models\User;
use Illuminate\Database\QueryException;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\TokenRepository;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function login(LoginUserRequest $request)
    {

        $loginCredentials=[
            'email' =>$request->email,
            'password' =>$request->password,
        ];

        try{
            //check if access credentials are valid
            if(!Auth::attempt($loginCredentials) ){
                return response()->json([
                    SystemData::Message=>SystemMessage::AuthLoginFailed,
                    SystemData::Success=>false],
                    HttpStatusCode::ClientErrorNotFound);
                }

            //check if user account_status and status are both active
            if(!auth()->user()->status || !auth()->user()->account_status){
                return response()->json([
                    SystemData::Message=>SystemMessage::AuthAccessSuspended,
                    SystemData::Success=>false],
                    HttpStatusCode::ClientErrorNotFound);
                }

                $user=User::find(Auth::id());
                $accessToken  =   $user->createToken('Personal Access Token')->accessToken;

                //show found record as object
            return response()->json([
                SystemData::Data=>UserResource::make($user),
                SystemData::AccessToken=> $accessToken,
                SystemData::Message=>SystemMessage::AuthLoginSuccess,
                SystemData::Success=>true]);


            }catch(QueryException $e){ // Catch all Query Errors
                return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Exception $e){     // Catch all General Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Error $e){          // Catch all Php Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }

    }

    public function logout(Request $request){

        //Get the tokenId use getTokenId function from AuthHelper.php in Helpers folder.
        $tokenId = getTokenId($request);

        try{
            // dd('hello');

            //Use the Token Repository to Revoke the Access Token
            $tokenRepository = app(TokenRepository::class);
            $tokenRepository->revokeAccessToken($tokenId);

            return response()->json([
            SystemData::Message=>SystemMessage::AuthLogout,
            SystemData::Success=>true]);

        }catch(QueryException $e){ // Catch all Query Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Exception $e){     // Catch all General Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Error $e){          // Catch all Php Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    public function checkTokenStatus(Request $request){

        try{

            //Get the tokenId use getTokenId function from AuthHelper.php in Helpers folder.
            if(strpos($request->header('authorization'),"Bearer")!==false){
                $tokenId = getTokenId($request);
            }else
            {
                $tokenId=0;
            }

            //check if token still good in the database
            $tokenStatus=DB::table('oauth_access_tokens')->where('id','=',$tokenId)
                                                                ->where('revoked','=',0)->first();

            if(is_null($tokenStatus)){
                return response()->json([
                    SystemData::TokenStatus=>0, // 0 means no longer valid
                    SystemData::Message=>SystemMessage::TokenNoLongerValid,
                    SystemData::Success=>false]);
            }

            return response()->json([
                SystemData::TokenStatus=>1, // 0 means no longer valid
                SystemData::Message=>SystemMessage::TokenStillValid,
                SystemData::Success=>true]);

        }catch(QueryException $e){ // Catch all Query Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Exception $e){     // Catch all General Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }catch(\Error $e){          // Catch all Php Errors
        return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }
    }


    public function forgotPassword(ForgotPasswordRequest $request)
    {

        try{
                $user=User::where('email','=',$request->email)->first();

                if (is_null($user)) {
                    return response()->json([
                           SystemData::Message => SystemMessage::UserNoRecordFound,
                           SystemData::Success => false],
                   HttpStatusCode::ClientErrorNotFound);
                }

                //create the reset_password_code
                $passwordResetCode =  fake()->numberBetween($min = 1111111, $max = 99999999);
                $user->update(['password_reset_code'=> $passwordResetCode ]);

                //email password reset link and password_reset_code
                //add these data before sending the email
                $userData=$user->getattributes();
                $userData['password_reset_url'] = env('FRONT_END_APP_RESET_PASSWORD_URL');
                $userData['password_reset_code'] =  $passwordResetCode;

                //send email for password reset
                Mail::to($user->email)->send(new ResetPasswordMail($userData));

                //show message
                return response()->json([
                    SystemData::Message => SystemMessage::AuthSendPasswordResetLink,
                    SystemData::Success => true]);


            }catch(QueryException $e){ // Catch all Query Errors
                return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Exception $e){     // Catch all General Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Error $e){          // Catch all Php Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }

    }

    public function resetPassword(ResetPasswordRequest $request)
    {

        try{
            $user=User::where('email','=',$request->email)
                        ->where('password_reset_code','=',$request->password_reset_code)
                        ->first();

            if (is_null($user)) {
                return response()->json([
                       SystemData::Message => SystemMessage::ResetPasswordFailed,
                       SystemData::Success => false],
               HttpStatusCode::ClientErrorNotFound);
            }

            //activate the new password and set reset_password_code to null
            $user->update(['password_reset_code'=> null, 'password' => Hash::make($request->new_password)]);

            //email password reset successful



            //show message
            return response()->json([
                SystemData::Message => SystemMessage::ResetPasswordSuccess,
                SystemData::Success => true]);


            }catch(QueryException $e){ // Catch all Query Errors
                return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Exception $e){     // Catch all General Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Error $e){          // Catch all Php Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }

    }

    public function changePassword(ChangePasswordRequest $request)
    {


        try{
            $user=User::find(auth()->user()->id);
            $usercurrentPassword = $user->password;

            if (is_null($user) || !Hash::check($request->old_password,$usercurrentPassword)) {
                return response()->json([
                       SystemData::Message => SystemMessage::ChangePasswordFailed,
                       SystemData::Success => false],
               HttpStatusCode::ClientErrorNotFound);
            }

            $user->update(['password'=> Hash::make($request->new_password)]);

            //show message
            return response()->json([
                SystemData::Message => SystemMessage::ResetPasswordSuccess,
                SystemData::Success => true]);


            }catch(QueryException $e){ // Catch all Query Errors
                return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Exception $e){     // Catch all General Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
            }catch(\Error $e){          // Catch all Php Errors
            return response()->json([ SystemData::Message =>$e->getMessage(),SystemData::Success=>false],HttpStatusCode::ServerErrorInternalServerError);
        }

    }

}
