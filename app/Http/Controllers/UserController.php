<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\User;
use App\Exports\UserExport;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Http\Requests\ViewUserRequest;
use Illuminate\Database\QueryException;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;
use App\Enums\FileName;
use App\Enums\SystemData;
use App\Http\Requests\UpdateUserProfileRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccessCredentialMail;

class UserController extends Controller
{
    public function index(ViewUserRequest $request)
    {

        //Validation is automatically done by
        //the ViewUserRequest class

        //extract page size
        $per_page = $request->input('per_page');

        $wildCard = ($request->filter_activatewildcard == 1) ? '%' : '';

        //Let's build our queries
        $dataQuery = User::FromCurrentAccount()->CustomSearch();

        //Check for Filters Begin -------------------------------------------------------//

        //Filter All COlumn
        if ($request->filter_allcolumn != '') {
            $dataQuery->whereraw('concat(users.id,users.first_name,users.last_name,users.email,users.phone,users.address1,users.address2,ccountries.country,users.subnational_entity,users.zip_postal_code,roles.role,users.created_at) LIKE ?', '%' . $request->filter_allcolumn . '%');
        }

        //Filter ID
        if ($request->filter_id != '') {
            $dataQuery->where('users.id', 'LIKE', $wildCard . $request->filter_id . '%');
        }

        //Filter type
        if ($request->filter_type != '') {
            $dataQuery->where('users.type', 'LIKE', $wildCard . $request->filter_type . '%');
        }

        //Filter First Name
        if ($request->filter_first_name != '') {
            $dataQuery->where('users.first_name', 'LIKE', $wildCard . $request->filter_first_name . '%');
        }

        //Filter Last Name
        if ($request->filter_last_name != '') {
            $dataQuery->where('users.last_name', 'LIKE', $wildCard . $request->filter_last_name . '%');
        }

        //Filter Email
        if ($request->filter_email != '') {
            $dataQuery->where('users.email', 'LIKE', $wildCard . $request->filter_email . '%');
        }

        //Filter Phone
        if ($request->filter_phone != '') {
            $dataQuery->where('users.phone', 'LIKE', $wildCard . $request->filter_phone . '%');
        }

        //Filter Address1
        if ($request->filter_address1 != '') {
            $dataQuery->where('users.address1', 'LIKE', $wildCard . $request->filter_address1 . '%');
        }

        //Filter Address2
        if ($request->filter_address2 != '') {
            $dataQuery->where('users.address2', 'LIKE', $wildCard . $request->filter_address2 . '%');
        }

        //Filter City
        if ($request->filter_city != '') {
            $dataQuery->where('ccountries.country', 'LIKE', $wildCard . $request->filter_city . '%');
        }

        //Filter subnational_entity
        if ($request->filter_subnational_entity != '') {
            $dataQuery->where('users.subnational_entity', 'LIKE', $wildCard . $request->filter_subnational_entity . '%');
        }

        //Filter Zip
        if ($request->filter_zip != '') {
            $dataQuery->where('users.zip_postal_code', 'LIKE', $wildCard . $request->filter_zip . '%');
        }

        //Filter role
        if ($request->filter_role != '') {
            $dataQuery->where('roles.role', 'LIKE', $wildCard . $request->filter_role . '%');
        }

        //Filter Created_at
        if ($request->filter_created_at != '') {
            $dataQuery->where('users.created_at', 'LIKE', $wildCard . $request->filter_created_at . '%');
        }

        //Check for Filters End -------------------------------------------------------//

        //Apply Sorting Mechanism Begin ----------------------------------------------//

        //Sort by ID
        if ($request->sort_id) {
            $dataQuery->orderBy('users.id', ($request->sort_id == 1) ? "asc" : "desc");
        }

        //Sort by Type
        if ($request->sort_type) {
            $dataQuery->orderBy('users.type', ($request->sort_type == 1) ? "asc" : "desc");
        }

        //Sort by First Name
        if ($request->sort_first_name) {
            $dataQuery->orderBy('users.first_name', ($request->sort_first_name == 1) ? "asc" : "desc");
        }

        //Sort by Last Name
        if ($request->sort_last_name) {
            $dataQuery->orderBy('users.last_name', ($request->sort_last_name == 1) ? "asc" : "desc");
        }

        //Sort by Email
        if ($request->sort_email) {
            $dataQuery->orderBy('users.email', ($request->sort_email == 1) ? "asc" : "desc");
        }

        //Sort by Phone
        if ($request->sort_phone) {
            $dataQuery->orderBy('users.phone', ($request->sort_phone == 1) ? "asc" : "desc");
        }

        //Sort by Address1
        if ($request->sort_address1) {
            $dataQuery->orderBy('users.address1', ($request->sort_address1 == 1) ? "asc" : "desc");
        }

        //Sort by Address2
        if ($request->sort_address2) {
            $dataQuery->orderBy('users.address2', ($request->sort_address2 == 1) ? "asc" : "desc");
        }

        //Sort by City
        if ($request->sort_city) {
            $dataQuery->orderBy('ccountries.country', ($request->sort_city == 1) ? "asc" : "desc");
        }

        //Sort by subnational_entity
        if ($request->sort_state) {
            $dataQuery->orderBy('users.subnational_entity', ($request->sort_subnational_entity == 1) ? "asc" : "desc");
        }

        //Sort by Zip
        if ($request->sort_zip) {
            $dataQuery->orderBy('users.zip_postal_code', ($request->sort_zip == 1) ? "asc" : "desc");
        }

        //Sort by roles
        if ($request->sort_role) {
            $dataQuery->orderBy('roles.role', ($request->sort_role == 1) ? "asc" : "desc");
        }

        //Sort by Created_at
        if ($request->sort_created_at) {
            $dataQuery->orderBy('users.created_at', ($request->sort_created_at == 1) ? "asc" : "desc");
        }

        //Apply Sorting Mechanism End -----------------------------------------==-----//

        try {

            //Lastly Execute Query and Paginate the result
            //paginate also perform counting of records
            //if you have 1 million record it is still fast as long as
            //you make use of proper table joins
            //use leftjoin if some tables most of the time has less than 1000 records
            // data from the main table
            $dataResult = $dataQuery->paginate($per_page);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }

        //Check if dataResult has record, if not throw message
        if ($dataResult->count() <= 0) {
            return response()->json(
                [
                    SystemData::Message => SystemMessage::UserNoRecordFound,
                    SystemData::Success => false
                ],
                HttpStatusCode::ClientErrorNotFound
            );
        }

        //Check if this request is for EXPORT.
        //If export_to is NOT '' means YES then export the data to target format
        if ($request->export_to != '') {
            switch (strtoupper($request->export_to)) {
                case FileName::EXCELFile:
                    return (new UserExport($dataResult))->download(FileName::UserExportFile . FileName::EXCELFileFormat, \Maatwebsite\Excel\Excel::XLSX, null);
                    break;
                case FileName::CSVFile:
                    return (new UserExport($dataResult))->download(FileName::UserExportFile . FileName::CSVFileFormat, \Maatwebsite\Excel\Excel::CSV, null);
                    break;

                default:
                    return response()->json(
                        [
                            SystemData::Message => SystemMessage::FileFormatNotSupported,
                            SystemData::Success => false
                        ],
                        HttpStatusCode::ClientErrorBadRequest
                    );
            }
        }

        return UserResource::collection($dataResult)->additional([
            SystemData::Message => SystemMessage::UserRecordRetrieved,
            SystemData::Success => true
        ]);
    }

    public function all()
    {
        //Show all records including those mark deleted
        $dataResult = User::withTrashed()->simplepaginate(100);
        return UserResource::collection($dataResult)->additional([
            SystemData::Message => SystemMessage::UserRecordRetrieved,
            SystemData::Success => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store(StoreUserRequest $request)
    {

        //Validation is automatically handled
        //by StoreUserRequest

        //check that the email is not an existing user.
        if (CheckIfUserEmailExist($request->email)) {
            return response()->json(
                [
                    SystemData::Message => SystemMessage::UserEmailAlreadyExist,
                    SystemData::Success => false
                ],
                HttpStatusCode::ClientErrorConflict
            );
        }

        //Pick Up only all those data from the request
        //that are needed to save
        $data = [
            "type" => $request->type,
            "account_id" => auth()->user()->account_id,
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "name" => $request->first_name . ' ' . $request->last_name,
            "email" => $request->email,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // default password is password
            "address1" => $request->address1,
            "address2" => $request->address2."dert",
            "subnational_entity" => $request->subnational_entity,
            "zip_postal_code" => $request->zip_postal_code,
            "city" =>$request->city,
            "country_id" =>$request->country_id,
            "phone" => $request->phone,
            "role_id" => $request->role_id,

        ];


        try {
            //Execute and return the newly created record
            $user = User::create($data);
            $user->country = getCountry($user->country_id)?->country;
            $user->role = getRole($user->role_id)->role;

            //Automatically send Mail to that user
            //Mail::to($user['email'])->send(new AccessCredentialMail($user));

            //return the resource with additional element
            return UserResource::collection([$user])->additional([
                SystemData::Message => SystemMessage::UserRecordCreated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Display the specified resource.
     */

    public function show($id)
    {
        try {

            $user = User::FromCurrentAccount()->CustomSearch()->find($id);

            if (is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //show found record as object
            return response()->json([
                SystemData::Data => UserResource::make($user),
                SystemData::Message => SystemMessage::UserRecordFound,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    public function showProfile()
    {
        try {

            $user = User::FromCurrentAccount()->CustomSearch()->find(auth()->user()->id);

            if (is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserNoRecordFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //show found record as object
            return response()->json([
                SystemData::Data => UserResource::make($user),
                SystemData::Message => SystemMessage::UserRecordFound,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Update the specified resource in storage.
     */

    public function update(UpdateUserRequest $request, $id)
    {
        //Validation is automatically handled
        //by the UpdateUserRequest

        //Pick Up only all those data from the request
        //that are needed to update
        $data = [
            "type" => $request->type,
            "account_id" => auth()->user()->account_id,
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "name" => $request->first_name . ' ' . $request->last_name,
            "email" => $request->email,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // default password is password
            "address1" => $request->address1,
            "address2" => $request->address2."dert",
            "subnational_entity" => $request->subnational_entity,
            "zip_postal_code" => $request->zip_postal_code,
            "city" =>$request->city,
            "country_id" =>$request->country_id,
            "phone" => $request->phone,
            "role_id" => $request->role_id,
        ];

        //Find and return the found record
        try {
            $user = User::FromCurrentAccount()->CustomSearch()->find($id);

            if (is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute update to update the record
            $user->update($data);
            $user = User::FromCurrentAccount()->CustomSearch()->find($id);

            //show found record as object
            return response()->json([
                SystemData::Data => UserResource::make($user),
                SystemData::Message => SystemMessage::UserRecordUpdated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    public function updateProfile(UpdateUserProfileRequest $request)
    {
        //Validation is automatically handled
        //by the UpdateUserRequest

        //Pick Up only all those data from the request
        //that are needed to update
        $data = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "name" => $request->first_name . ' ' . $request->last_name,
            "email" => $request->email,
            "address1" => $request->address1,
            "subnational_entity" => $request->subnational_entity,
            "zip_postal_code" => $request->zip_postal_code,
            "city" =>$request->city,
            "country_id" =>$request->country_id,
            "phone" => $request->phone,
        ];

        //Find and return the found record
        try {

            //check first if the new email does not exist yet in the system
            // if it exist then abort the update
            $user = User::where('id','!=',auth()->user()->id)
                          ->where('email','=',$request->email)->first();

            if (!is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserEmailAlreadyExist,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            $user = User::FromCurrentAccount()->CustomSearch()->find(auth()->user()->id);

            if (is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserNoRecordFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute update to update the record
            $user->update($data);
            $user = User::FromCurrentAccount()->CustomSearch()->find(auth()->user()->id);

            //show found record as object
            return response()->json([
                SystemData::Data => UserResource::make($user),
                SystemData::Message => SystemMessage::UserRecordUpdated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Remove the specified resource from storage.
     */

    public function destroy($id)
    {
        try {
            //Before delete make sure to check first if this client ID
            //is not yet been used as referrence by accounts record

            if (!is_null(null)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserCanNotDelete . $id . SystemMessage::AlreadyBeenUsed,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            $user = User::FromCurrentAccount()->CustomSearch()->find($id);

            if (is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute Delete and return the deleted record
            $user->delete();


            //show deleted record as object
            return response()->json([
                SystemData::Data => UserResource::make($user),
                SystemData::Message => SystemMessage::UserRecordDeleted,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }


    public function undestroy($id)
    {
        try {

            $user = User::FromCurrentAccount()->CustomSearch()->onlyTrashed()->find($id);

            if (is_null($user)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::UserID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute Restore and return the restored record
            $user->restore();

            //show undeleted record as object
            return response()->json([
                SystemData::Data => UserResource::make($user),
                SystemData::Message => SystemMessage::UserRecordRestored,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }


    public function stats()
    {
        try {

            $userActiveRecords = User::FromCurrentAccount()->count();
            $userSoftDeletedRecords = User::FromCurrentAccount()->onlyTrashed()->count();
            $userTotalRecords = $userActiveRecords + $userSoftDeletedRecords;

            //return the Resource with additional element
            return response()->json(
                [
                    SystemData::Data => [
                        SystemData::ActiveRecords => $userActiveRecords,
                        SystemData::SoftDeletedRecords => $userSoftDeletedRecords,
                        SystemData::TotalRecords => $userTotalRecords
                    ],
                    SystemData::Message => SystemMessage::StatsRetrieved,
                    SystemData::Success => true
                ],
                HttpStatusCode::SuccessOK
            );
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }
}
