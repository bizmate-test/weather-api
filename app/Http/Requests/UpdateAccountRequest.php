<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;

class UpdateAccountRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */

    public function rules(): array
    {
        return [
            "first_name" => "required|max:255",
            "middle_name" => "max:255",
            "last_name" => "required|max:255",
            "email" => "email|max:255",
            "website" => "max:255",
            "phone1" => "max:50",
            "phone2" => "max:50",
            "phone3" => "max:50",
            "fax" => "max:50",
            "address1" => "max:255",
            "address2" => "max:255",
            "zip_postal_code" => "max:50",
            "city" => "max:255",
            "subnational_entity" => "max:255",
            "country_id" => "nullable|numeric",
            "note" => "max:255",
            "status" => "required|numeric|max:1",
            "tag" =>"max:255",
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'data'      => $validator->errors(),
            'message'   => SystemMessage::ValidationError,
            'success'   => false,
        ],
        HttpStatusCode::ClientErrorBadRequest));
    }
}
