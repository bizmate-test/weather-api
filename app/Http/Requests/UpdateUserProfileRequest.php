<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;

class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "first_name" => "required|max:100",
            "last_name" => "required|max:100",
            "email" => "required|email|max:100",
            "address1" => "required|max:255",
            "city" => "required|max:255",
            "zip_postal_code" => "required|max:50",
            "subnational_entity" => "required|max:255",
            "country_id" => "required|numeric",
            "country_id" => "required|numeric",
            "phone" => "required|max:50",
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'data'      => $validator->errors(),
            'message'   => SystemMessage::ValidationError,
            'success'   => false,
        ],
        HttpStatusCode::ClientErrorBadRequest));
    }
}
