<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "type" => "required|numeric",
            "first_name" => "required|max:100",
            "last_name" => "required|max:100",
            "email" => "required|email|max:100",
            "address1" => "required|max:255",
            "address2" => "max:255",
            "country_id" => "nullable|numeric",
            "subnational_entity" => "required|max:255",
            "zip_postal_code" => "required|max:50",
            "phone" => "required|max:50",
            "role_id" => "required|numeric",
            "account_id" => "required|numeric",
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'data'      => $validator->errors(),
            'message'   => SystemMessage::ValidationError,
            'success'   => false,
        ],
        HttpStatusCode::ClientErrorBadRequest));
    }
}
