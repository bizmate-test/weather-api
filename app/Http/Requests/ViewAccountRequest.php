<?php

namespace App\Http\Requests;

use App\Enums\AccountType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;
use App\Enums\SystemSetting;

class ViewAccountRequest extends FormRequest
{
     /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {

        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            //For all sort parameters numeric 1 means ascending | 2 means descending
            "per_page" => "required|numeric|min:".SystemSetting::AllowedPageMinValue."|max:".SystemSetting::AllowedPageMaxValue."",
            "sort_id" => "numeric:min:1|max:2",
            "sort_first_name" => "numeric:min:1|max:2",
            "sort_middle_name" => "numeric:min:1|max:2",
            "sort_last_name" => "numeric:min:1|max:2",
            "sort_email" => "numeric:min:1|max:2",
            "sort_website" => "numeric:min:1|max:2",
            "sort_phone1" => "numeric:min:1|max:2",
            "sort_phone2" => "numeric:min:1|max:2",
            "sort_phone3" => "numeric:min:1|max:2",
            "sort_fax" => "numeric:min:1|max:2",
            "sort_address1" => "numeric:min:1|max:2",
            "sort_address2" => "numeric:min:1|max:2",
            "sort_city" => "numeric:min:1|max:2",
            "sort_subnational_entity" => "numeric:min:1|max:2",
            "sort_zip_postal_code" => "numeric:min:1|max:2",
            "sort_country_id" => "numeric:min:1|max:2",
            "sort_status" => "numeric:min:1|max:2",
            "sort_tag" => "numeric:min:1|max:2",
            "sort_created_at" => "numeric:min:1|max:2",
            "count_only" => "nullable|numeric:min:0|max:1",
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'data'      => $validator->errors(),
            'message'   => SystemMessage::ValidationError,
            'success'   => false,
        ],
        HttpStatusCode::ClientErrorBadRequest));
    }
}
