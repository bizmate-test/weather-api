<?php

namespace App\Http\Resources;

use App\Enums\Status;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        //return parent::toArray($request);
        return[
            'id'=>$this->id,
            'first_name'=>$this->first_name,
            'middle_name'=>$this->middle_name,
            'last_name'=>$this->last_name,
            'email'=>$this->email,
            'website'=>$this->website,
            'phone1'=>$this->phone1,
            'phone2'=>$this->phone2,
            'phone3'=>$this->phone3,
            'fax'=>$this->fax,
            'address1'=>$this->address1,
            'address2'=>$this->address2,
            'zip_postal_code'=>$this->zip_postal_code,
            'city'=>$this->city,
            'subnational_entity'=>$this->subnational_entity,
            'country_id'=>$this->country_id,
            'country'=>$this->country,
            'note'=>$this->note,
            'status'=>Status::getDescription($this->status),
            'tag'=>$this->tag,
            'created_at'=>$this->created_at->toDateTimeString(),
         ];
    }
}
