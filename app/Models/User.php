<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'type',
        'account_id',
        'first_name',
        'last_name',
        'name',
        'email',
        'phone',
        'password',
        'address1',
        'address2',
        'city',
        'subnational_entity',
        'zip_postal_code',
        'country_id',
        'role_id',
        'client_id',
        'picture',
        'status',
        'account_status',
        'activation_code',
        'password_reset_code',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $selectedFields=[
        'users.id',
        'users.account_id',
        'first_name',
        'last_name',
        'name',
        'email',
        'phone',
        'address1',
        'address2',
        'zip_postal_code',
        'city',
        'subnational_entity',
        'country_id',
        'countries.country',
        'role_id',
        'roles.role',
        'users.created_at'
         ];

    public function scopeFromCurrentAccount($query)
    {
        //always check that records belong to the account_id
        //of the currently authenticated user
        return $query->where('users.account_id','=',auth()->user()->account_id);
    }

     public function scopeCustomSearch($query)
    {
         return $query->select($this->selectedFields)
         ->leftjoin('countries', 'users.country_id', '=', 'countries.id')
         ->leftjoin('roles', 'users.role_id', '=', 'roles.id');
    }
}
