<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Account>
 */
class AccountFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => fake()->firstName(),
            'last_name' => fake()->lastName(),
            'email' => fake()->unique()->safeEmail(),
            'website' => fake()->domainName(),
            'phone1' => fake()->e164PhoneNumber(),
            'phone2' => fake()->e164PhoneNumber(),
            'phone3' => fake()->e164PhoneNumber(),
            'fax'  => fake()->e164PhoneNumber(),
            'address1' => fake()->streetAddress(),
            'address2' => fake()->address(),
            'city' => fake()->city(),
            'subnational_entity' => fake()->state(),
            'zip_postal_code' =>fake()->postcode(),
            'country_id' => fake()->numberBetween($min = 1, $max = 200),
            'note' => fake()->sentence(),
            'status' => fake()->numberBetween($min = 0, $max = 1),
            'tag' => fake()->word(),
        ];
    }
}
