<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('account_id');
            $table->string('role');
            $table->string('description',255)->nullable();
            $table->json('permission')->nullable();
            $table->timestamps();
            $table->softDeletes();

            //set Normal indexes
            $table->index('account_id');
            $table->index('role');
            $table->index('description');
            $table->index('created_at');
            $table->index('deleted_at');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('roles');
    }
};
