<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
        /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('website')->nullable();
            $table->string('phone1',50)->nullable();
            $table->string('phone2',50)->nullable();
            $table->string('phone3',50)->nullable();
            $table->string('fax',50)->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('zip_postal_code',50)->nullable();
            $table->string('city')->nullable();
            $table->string('subnational_entity')->nullable();
            $table->string('country_id')->nullable();
            $table->string('note')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('tag')->nullable();
            $table->timestamps();
            $table->softDeletes();

            //set Normal indexes
             $table->index('first_name');
             $table->index('middle_name');
             $table->index('last_name');
             $table->index('email');
             $table->index('website');
             $table->index('phone1');
             $table->index('phone2');
             $table->index('phone3');
             $table->index('fax');
             $table->index('address1');
             $table->index('address2');
             $table->index('zip_postal_code');
             $table->index('city');
             $table->index('subnational_entity');
             $table->index('country_id');
             $table->index('note');
             $table->index('status');
             $table->index('tag');
             $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('accounts');
    }
};
