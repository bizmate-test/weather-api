<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //create 1 active account
        Account::factory()->create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'superadmin@email.com',
            'phone1' => '1234567890',
            'address1' => '123 main street',
            'address2' => 'suite 44',
            'city' => 'South Lancaster',
            'subnational_entity' => fake()->state(),
            'zip_postal_code' =>'01561',
            'country_id' => 1,
            'status' => 1,
            ]);
        //fake the rest
        Account::factory(49)->create();

    }
}
