<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        //Execute Seeds for Countries
        $this->call(CountrySeeder::class);

        //Execute Seeds for Role
        $this->call(RoleSeeder::class);

        //Execute Seeds for User
        $this->call(UserSeeder::class);

        //Execute Seeds for Account
        $this->call(AccountSeeder::class);

        //if you want to seed Account Model only do this
        //php artisan db:seed --class=AccountSeeder

    }
}
