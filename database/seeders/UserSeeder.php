<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
         //create 1 super admin
         User::factory()->create([
            'account_id' => 1,
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'name' => 'Super Admin',
            'email' => 'superadmin@email.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'phone' => '1234567890',
            'address1' => '123 main street',
            'address2' => 'suite 44',
            'city' => 'South Lancaster',
            'subnational_entity' => fake()->state(),
            'zip_postal_code' =>'01561',
            'country_id' => 1,
            'role_id' => 1,
            'status' => 1,
            'account_status' => 1,
         ]);

        //fake the rest
        User::factory(29)->create();

    }
}
